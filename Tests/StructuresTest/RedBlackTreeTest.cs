﻿using System.Collections.Generic;
using Algorithms.DataStructure;
using Algorithms.DataStructure.Trees.RedBlack;
using NUnit.Framework;

namespace Tests.StructuresTest
{
    public class RedBlackTreeTest
    {
        [Test]
        public void ReadBlackTree_AddElements_ShouldBeSuccess()
        {
            var list = new List<int> { 9,5,8,15,16,11 };
            var rbtree = new RedBlackTree<int>();
            foreach (var item in list)
            {
                rbtree.Add(item);
            }
            
        }
    }
}