﻿using System.Collections.Generic;
using Algorithms.DataStructure;
using NUnit.Framework;

namespace Tests.StructuresTest
{
    public class BinaryHeapTest
    {
        [Test]
        public void BinaryHeap_ExtractMaxElement_ShouldBeSuccess()
        {
            var list = new List<int> { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
            var binaryHeap = new BinaryHeap<int>(list);
            var max = binaryHeap.ExtractPeek();
            Assert.AreEqual(max, 16);
        }

        [Test]
        public void BinaryHeap_AddMin_ThenExtractMinElement_ShouldBeSuccess()
        {
            var list = new List<int> { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
            var binaryHeap = new BinaryHeap<int>(list, BinaryHeapType.MinHeapType);
            binaryHeap.InsertKey(0);
            var min = binaryHeap.ExtractPeek();
            Assert.AreEqual(min, 0);
        }

        [Test]
        public void BinaryHeap_AddSomeElements_ThenExtractMinElement_ShouldBeSuccess()
        {
            var list = new List<int> { 4, 31, 16, 9, 10, 14, 8, 7 };
            var binaryHeap = new BinaryHeap<int>(list, BinaryHeapType.MinHeapType);
            binaryHeap.InsertKey(8);
            binaryHeap.InsertKey(22);
            binaryHeap.InsertKey(0);
            var min = binaryHeap.ExtractPeek();
            Assert.AreEqual(min, 0);
            Assert.AreEqual(binaryHeap.Peek(), 4);
        }

    }
}