﻿using System.Collections.Generic;
using Algorithms.Sorting;
using NUnit.Framework;

namespace Tests.AlgorithmsTests.SortingTests
{
    public class InsertionSortingTests
    {
        [Test]
        public void InsertionSortingAsc_ShouldBeSuccess()
        {
            var list = new List<int> {1, 12, 3, 45, 36, 87, 67, 9};
            list.InsertionSorterAsc();
            Assert.That(list, Is.Ordered.Ascending);
        }
        [Test]
        public void InsertionSortingDesc_ShouldBeSuccess()
        {
            var list = new List<int> { 1, 12, 3, 45, 36, 87, 67, 9 };
            list.InsertionSorterDesc();
            Assert.That(list, Is.Ordered.Descending);
        }
    }
}