﻿using System.Collections.Generic;
using Algorithms.Common;
using Algorithms.Sorting;
using NUnit.Framework;
using Tests.Common;

namespace Tests.AlgorithmsTests.SortingTests
{
    public class QuickSorterTest
    {
        private List<EmployeeTest> _employees;

        [SetUp]
        public void SetUp()
        {
            _employees = new List<EmployeeTest>
            {
                new EmployeeTest {Name = "Vasilii", Age = 34},
                new EmployeeTest {Name = "Pert", Age = 35},
                new EmployeeTest {Name = "Ivan", Age = 26},
                new EmployeeTest {Name = "Stepan", Age = 18},
                new EmployeeTest {Name = "Jon", Age = 54}
            };
        }

        [Test]
        public void QuickSortingAsc_ShouldBeSuccess()
        {
            _employees.QuickSort(x => x.Name);
            Assert.That(_employees, Is.Ordered.Ascending.By("Name"));
        }

        [Test]
        public void QuickSortingDesc_ShouldBeSuccess()
        {
            _employees.QuickSort(x => x.Age, OrderBy.Desc);
            Assert.That(_employees, Is.Ordered.Descending.By("Age"));
        }
    }
}