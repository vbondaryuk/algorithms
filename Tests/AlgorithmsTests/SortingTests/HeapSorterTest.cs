﻿using System.Collections.Generic;
using Algorithms.Common;
using Algorithms.DataStructure;
using Algorithms.Sorting;
using NUnit.Framework;
using Tests.Common;

namespace Tests.AlgorithmsTests.SortingTests
{
    public class HeapSorterTest
    {
        private List<EmployeeTest> _employees;

        [SetUp]
        public void SetUp()
        {
            _employees = new List<EmployeeTest>
            {
                new EmployeeTest {Name = "Vasilii", Age = 34},
                new EmployeeTest {Name = "Pert", Age = 35},
                new EmployeeTest {Name = "Ivan", Age = 26},
                new EmployeeTest {Name = "Stepan", Age = 18},
                new EmployeeTest {Name = "Jon", Age = 54}
            };
        }

        [Test]
        public void BinaryHeapSortingAsc_ShouldBeSuccess()
        {
            var list = new List<int> {4, 1, 3, 2, 16, 9, 10, 14, 8, 7};
            list.BinaryHeapSort(x => x);
            Assert.That(list, Is.Ordered.Ascending);
        }

        [Test]
        public void BinaryHeapSortingDesc_ShouldBeSuccess()
        {
            var list = new List<int> { 4, 1, 3, 2, 16, 9, 10, 14, 8, 7 };
            list.BinaryHeapSort(x => x, OrderBy.Desc);
            Assert.That(list, Is.Ordered.Descending);
        }

        [Test]
        public void HeapSortingStructure_ShouldBeSorted()
        {
            _employees.BinaryHeapSort(x => x.Name);
            Assert.That(_employees, Is.Ordered.Ascending.By("Name"));
        }
    }
}