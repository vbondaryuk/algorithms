﻿using System.Collections.Generic;
using Algorithms.Sorting;
using NUnit.Framework;

namespace Tests.AlgorithmsTests.SortingTests
{
    public class BubleSorterTest
    {
        [Test]
        public void BubleSortingAsc_ShouldBeSuccess()
        {
            var list = new List<int> { 1, 12, 3, 45, 36, 87, 67, 9 };
            list.BubleSort();
            Assert.That(list, Is.Ordered.Ascending);
        }
    }
}