﻿using System.Collections.Generic;
using Algorithms.Common;
using Algorithms.Sorting;
using NUnit.Framework;

namespace Tests.AlgorithmsTests.SortingTests
{
    public class MergeSorterTest
    {
        [Test]
        public void MergeSortAsc_ShouldBeSuccess()
        {
             var list = new List<int> {1, 12, 3, 45, 36, 87, 67, 9};
            list.MergeSort(x=>x);
            Assert.That(list, Is.Ordered.Ascending);
        }

        [Test]
        public void MergeSortAsc_String_ShouldBeSuccess()
        {
            var list = new List<string> { "A", "COM", "B", "Z", "XYZ", "QW"};
            list.MergeSort(x => x);
            Assert.That(list, Is.Ordered.Ascending);
        }

        [Test]
        public void MergeSortDesc_String_ShouldBeSuccess()
        {
            var list = new List<string> { "A", "COM", "B", "Z", "XYZ", "QW" };
            list.MergeSort(x => x, OrderBy.Desc);
            Assert.That(list, Is.Ordered.Descending);
        }
    }
}