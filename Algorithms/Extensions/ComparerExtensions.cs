﻿using System.Collections.Generic;

namespace Algorithms.Extensions
{
    public static class ComparerExtensions
    {
        public static bool IsEquals<T>(this IComparer<T> comparer, T left, T right)
        {
            return comparer.Compare(left, right) == 0;
        }
        public static bool IsFirstGreaterThenSecond<T>(this IComparer<T> comparer, T left, T right)
        {
            return comparer.Compare(left, right) > 0;
        }
        public static bool IsFirstGreaterThenOrEqualsToSecont<T>(this IComparer<T> comparer, T left, T right)
        {
            return comparer.Compare(left, right) >= 0;
        }
        public static bool IsFirstLessThenSecond<T>(this IComparer<T> comparer, T left, T right)
        {
            return comparer.Compare(left, right) < 0;
        }
        public static bool IsFirstLessThenOrEqualsToSecond<T>(this IComparer<T> comparer, T left, T right)
        {
            return comparer.Compare(left, right) <= 0;
        }

    }
}