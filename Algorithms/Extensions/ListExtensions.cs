﻿using System.Collections.Generic;

namespace Algorithms.Extensions
{
    public static class ListExtensions
    {
        public static void Swap<T>(this IList<T> list, int fromIndex, int toIndex)
        {
            if (fromIndex == toIndex)
                return;
            T temp = list[fromIndex];
            list[fromIndex] = list[toIndex];
            list[toIndex] = temp;
        }
    }
}