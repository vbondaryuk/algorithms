﻿namespace Algorithms.DataStructure.Trees.RedBlack
{
    public class RedBlackNode<T>
    {
        public RedBlackColor Color { get; set; }
        public RedBlackNode<T> Parent { get; set; }
        public RedBlackNode<T> LeftNode { get; set; }
        public RedBlackNode<T> RightNode { get; set; }
        public T Item { get; set; }
        public bool IsExistsParentNode => Parent != null;
        public bool IsExistsLeftNode => LeftNode != null;
        public bool IsExistsRightNode => RightNode != null;
        public bool IsRed => Color == RedBlackColor.Red;
        public RedBlackNode<T> GrandParent => Parent?.Parent;

        public RedBlackNode<T> Uncle
        {
            get
            {
                var grandParent = GrandParent;
                if (grandParent == null)
                    return null;
                return Parent == grandParent.LeftNode ? grandParent.RightNode : grandParent.LeftNode;
            }
        }

        public RedBlackNode(T item) : this(item, RedBlackColor.Red)
        {
        }

        public RedBlackNode(T item, RedBlackColor color)
        {
            Item = item;
            Color = color;
        }
    }
}