﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Algorithms.DataStructure.Trees.RedBlack
{
    public class RedBlackTree<T> : IEnumerable<T>
    {
        private readonly Comparer<T> _comparer;
        private RedBlackNode<T> _root;

        public int Count { get; private set; }

        public RedBlackTree() : this(Comparer<T>.Default)
        {
        }

        public RedBlackTree(Comparer<T> comparer)
        {
            _comparer = comparer ?? Comparer<T>.Default;
        }

        public void Add(T item)
        {
            if (_root == null)
            {
                _root = new RedBlackNode<T>(item, RedBlackColor.Black);
                Count++;
                return;
            }

            RedBlackNode<T> currentNode = _root;
            RedBlackNode<T> parent = null;
            int compareIndicating = 0;
            while (currentNode != null)
            {
                compareIndicating = _comparer.Compare(item, currentNode.Item);
                if (compareIndicating == 0)
                    throw new ArgumentException("Node is already exists");
                // split a 4-node into two 2-nodes                
//                if (Is4Node(current))
//                {
//                    Split4Node(current);
//                    // We could have introduced two consecutive red nodes after split. Fix that by rotation.
//                    if (IsRed(parent))
//                    {
//                        InsertionBalance(current, ref parent, grandParent, greatGrandParent);
//                    }
//                }
                parent = currentNode;
                currentNode = compareIndicating > 0 ? currentNode.RightNode : currentNode.LeftNode;
            }

            var node = new RedBlackNode<T>(item) {Parent = parent};
            if (node.Parent != null)
            {
                if (compareIndicating > 0)
                    node.Parent.RightNode = node;
                else
                    node.Parent.LeftNode = node;
            }

            Count++;
            BalanceTreeAfterInsertion(node);
        }

        //https://en.wikipedia.org/wiki/Red–black_tree#Insertion
        private void BalanceTreeAfterInsertion(RedBlackNode<T> node)
        {
            if (!node.IsExistsParentNode)
            {
                node.Color = RedBlackColor.Black;
                return;
            }
            if (!node.Parent.IsRed)
                return;

            if (node.Uncle?.Color == RedBlackColor.Red && node.Parent.Color == RedBlackColor.Red)
            {
                node.Parent.Color = RedBlackColor.Black;
                node.Uncle.Color = RedBlackColor.Black;
                node.GrandParent.Color = RedBlackColor.Red;
                BalanceTreeAfterInsertion(node.GrandParent);
                return;
            }
            if (node == node.Parent.RightNode && node.Parent == node.GrandParent.LeftNode)
            {
                RotateToLeft(node.Parent);
                node = node.LeftNode;
            }else if (node == node.Parent.LeftNode && node.Parent == node.GrandParent.LeftNode)
            {
                RotateToRight(node.Parent);
                node = node.RightNode;
            }
            node.Parent.Color = RedBlackColor.Black;
            node.GrandParent.Color = RedBlackColor.Red;
            if (node == node.Parent.LeftNode)
            {
                RotateToRight(node.GrandParent);
            }
            else
            {
                RotateToLeft(node.GrandParent);
            }
            _root.Color = RedBlackColor.Black;
        }
        private void BalanceTreeAfterInsertionSecond(RedBlackNode<T> node)
        {
            if (!node.IsExistsParentNode)
            {
                node.Color = RedBlackColor.Black;
                return;
            }
            if (!node.Parent.IsRed)
                return;
            while (node != _root && node.Parent.Color == RedBlackColor.Red)
            {
                if (node.Uncle?.Color == RedBlackColor.Red)
                {
                    node.Parent.Color = RedBlackColor.Black;
                    node.Uncle.Color = RedBlackColor.Black;
                    node.GrandParent.Color = RedBlackColor.Red;
                    node = node.GrandParent;
                    continue;
                }

                if (node.Parent == node.GrandParent.LeftNode)
                {
                    if (node == node.Parent.RightNode)
                    {
                        node = node.Parent;
                        RotateToLeft(node);
                    }
                    node.Parent.Color = RedBlackColor.Black;
                    node.GrandParent.Color = RedBlackColor.Red;
                    RotateToRight(node.GrandParent);
                }
                else
                {
                    if (node == node.Parent.LeftNode)
                    {
                        node = node.Parent;
                        RotateToRight(node);
                    }
                    node.Parent.Color = RedBlackColor.Black;
                    node.GrandParent.Color = RedBlackColor.Red;
                    RotateToLeft(node.GrandParent);
                }
            }
            _root.Color = RedBlackColor.Black;
        }

        #region Rotation

        //      |              |
        //      X              Y
        //     / \            / \
        //    a   Y    ==>   X   c
        //       / \        / \
        //      b   c      a   b
        private void RotateToLeft(RedBlackNode<T> node)
        {
            if (!node.IsExistsRightNode)
                return;
            var isRootNode = node == _root;
            var pivot = node.RightNode;

            pivot.Parent = node.Parent;

            if (node.IsExistsParentNode)
            {
                if (node.Parent.LeftNode == node)
                    node.Parent.LeftNode = pivot;
                else
                    node.Parent.RightNode = pivot;
            }

            node.RightNode = pivot.LeftNode;
            if (pivot.LeftNode != null)
                pivot.LeftNode.Parent = node;

            pivot.LeftNode = node;
            node.Parent = pivot;

            if (isRootNode)
                _root = pivot;
        }

        //
        //      |             |         
        //      Y             X         
        //     / \           / \        
        //    X   c   ==>   a   Y  
        //   / \               / \    
        //  a   b             b   c  
        //
        private void RotateToRight(RedBlackNode<T> node)
        {
            if (!node.IsExistsLeftNode)
                return;
            var isRootNode = node == _root;
            var pivot = node.LeftNode;

            pivot.Parent = node.Parent;
            if (node.IsExistsParentNode)
            {
                if (node.Parent.LeftNode == node)
                    node.Parent.LeftNode = pivot;
                else
                    node.Parent.RightNode = pivot;
            }

            node.LeftNode = pivot.RightNode;
            if (pivot.IsExistsRightNode)
                pivot.RightNode.Parent = node;

            pivot.RightNode = node;
            node.Parent = pivot;

            if (isRootNode)
                _root = pivot;
        }

        #endregion

        public IEnumerator<T> GetEnumerator()
        {
            throw new System.NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}