﻿namespace Algorithms.DataStructure.Trees.RedBlack
{
    public enum RedBlackColor
    {
        Red,
        Black
    }
}