﻿using System.Collections.Generic;
using System.Linq;
using Algorithms.Extensions;

namespace Algorithms.DataStructure
{
    public enum BinaryHeapType
    {
        MinHeapType,
        MaxHeapType
    }

    public class BinaryHeap<TElement>
    {
        private readonly IList<TElement> _source;

        private readonly IComparer<TElement> _comparer;

        public int Count => _source.Count;

        public BinaryHeapType BinaryHeapType { get; }

        public BinaryHeap() : this(new List<TElement>())
        {
        }

        public BinaryHeap(IList<TElement> source, BinaryHeapType binaryHeapType = BinaryHeapType.MaxHeapType,
            Comparer<TElement> comparer = null)
        {
            _source = source.ToList();
            BinaryHeapType = binaryHeapType;
            _comparer = comparer ?? Comparer<TElement>.Default;
            BuildHeap();
        }

        public TElement Peek()
        {
            var maxElement = _source[0];
            return maxElement;
        }

        public TElement ExtractPeek()
        {
            var maxElement = Peek();
            _source.Swap(0, Count - 1);
            _source.RemoveAt(Count - 1);
            if (BinaryHeapType == BinaryHeapType.MaxHeapType)
            {
                MaxHeapify(_source, 0, Count);
            }
            else
            {
                MinHeapify(_source, 0, Count);
            }
            return maxElement;
        }

        public void InsertKey(TElement key)
        {
            _source.Add(key);
            if (BinaryHeapType == BinaryHeapType.MaxHeapType)
            {
                IncreaseKey(Count - 1);
            }
            else
            {
                DecreaseKey(Count - 1);
            }
        }

        private void IncreaseKey(int indexKey)
        {
           while (indexKey > 0 && _comparer.IsFirstLessThenSecond(_source[Parent(indexKey)], _source[indexKey]))
           {
               var parentIndex = Parent(indexKey);
                _source.Swap(indexKey, parentIndex);
                indexKey = parentIndex;
            }
        }
        private void DecreaseKey(int indexKey)
        {
            while (indexKey > 0 && _comparer.IsFirstGreaterThenSecond(_source[Parent(indexKey)], _source[indexKey]))
            {
                var parentIndex = Parent(indexKey);
                _source.Swap(indexKey, parentIndex);
                indexKey = parentIndex;
            }
        }

        private int Parent(int i)
        {
            return (i - 1)/2;
        }

        private int Left(int i)
        {
            return (i << 1) + 1;
        }

        private int Right(int i)
        {
            return Left(i) + 1;
        }

        private void BuildHeap()
        {
            var i = (Count - 1)/2;
            while (i >= 0)
            {
                if (BinaryHeapType == BinaryHeapType.MaxHeapType)
                {
                    MaxHeapify(_source, i--, Count);
                }
                else
                {
                    MinHeapify(_source, i--, Count);
                }
            }
        }

        private void MaxHeapify(IList<TElement> source, int i, int heapSize)
        {
            var left = Left(i);
            var right = Right(i);
            int largest = i;
            if (left < heapSize && _comparer.IsFirstGreaterThenSecond(source[left], source[largest]))
            {
                largest = left;
            }
            if (right < heapSize && _comparer.IsFirstGreaterThenSecond(source[right], source[largest]))
            {
                largest = right;
            }
            if (largest != i)
            {
                source.Swap(i, largest);
                MaxHeapify(source, largest, heapSize);
            }
        }

        private void MinHeapify(IList<TElement> source, int i, int heapSize)
        {
            var left = Left(i);
            var right = Right(i);
            int largest = i;
            if (left < heapSize && _comparer.IsFirstLessThenSecond(source[left], source[largest]))
            {
                largest = left;
            }
            if (right < heapSize && _comparer.IsFirstLessThenSecond(source[right], source[largest]))
            {
                largest = right;
            }
            if (largest != i)
            {
                source.Swap(i, largest);
                MinHeapify(source, largest, heapSize);
            }
        }
    }
}