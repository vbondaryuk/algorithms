﻿using System.Collections.Generic;
using Algorithms.Extensions;

namespace Algorithms.Sorting
{
    public static class BubleSorter
    {
        public static void BubleSort<T>(this List<T> list, IComparer<T> comparer = null)
        {
            comparer = comparer ?? Comparer<T>.Default;
            var length = list.Count;
            var swapped = false;
            for (int i = 0; i < length; i++)
            {
                for (int j = 0; j < length - i - 1; j++)
                {
                    if (comparer.Compare(list[j], list[j + 1]) < 0) 
                        continue;
                    list.Swap(j, j + 1);
                    swapped = true;
                }
                if(!swapped)
                    break;
            }
        }
    }
}