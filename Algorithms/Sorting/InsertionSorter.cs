﻿using System.Collections.Generic;

namespace Algorithms.Sorting
{
    public static class InsertionSorter
    {
        public static void InsertionSorterAsc<T>(this List<T> list, Comparer<T> comparer = null)
        {
            comparer = comparer ?? Comparer<T>.Default;
            for (int j = 1; j < list.Count; j++)
            {
                T key = list[j];
                var i = j - 1;
                while (i >= 0 && comparer.Compare(list[i], key) > 0)
                {
                    list[i + 1] = list[i];
                    i--;
                }
                list[i + 1] = key;
            }
        }
        public static void InsertionSorterDesc<T>(this List<T> list, Comparer<T> comparer = null)
        {
            comparer = comparer ?? Comparer<T>.Default;
            for (var j = list.Count - 2; j >= 0; j--)
            {
                T key = list[j];
                var i = j + 1;
                while (i < list.Count && comparer.Compare(list[i], key) > 0)
                {
                    list[i - 1] = list[i];
                    i++;
                }
                list[i - 1] = key;
            }
        }
    }
}