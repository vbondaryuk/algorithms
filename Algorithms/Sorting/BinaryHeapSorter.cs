﻿using System;
using System.Collections.Generic;
using Algorithms.Common;
using Algorithms.Extensions;

namespace Algorithms.Sorting
{
   public static class BinaryHeapSorter
    {
        public static void BinaryHeapSort<TElement, TKey>(
            this List<TElement> source, Func<TElement, TKey> selector,
            OrderBy orderBy = OrderBy.Asc, IComparer<TKey> comparer = null)
        {
            var orderParameters = new OrderParameters<TElement, TKey>
            {
                OrderBy = orderBy,
                Selector = selector,
                Comparer = comparer ?? Comparer<TKey>.Default
            };
            source.BuildHeap(orderParameters);
            source.Sort(orderParameters);
        }

        private static int Left(int i)
        {
            return (i << 1) + 1;
        }

        private static int Right(int i)
        {
            return Left(i) + 1;
        }
        private static void Sort<TElement, TKey>(this List<TElement> source, OrderParameters<TElement, TKey> orderParameters)
        {
            var i = source.Count - 1;
            while (i >= 0)
            {
                source.Swap(0, i);
                if (OrderBy.Asc == orderParameters.OrderBy)
                {
                    source.MaxHeapify(0, i, orderParameters);
                }
                else
                {
                    source.MinHeapify(0, i, orderParameters);
                }
                i--;
            }
        }

        private static void BuildHeap<TElement, TKey>(this List<TElement> source, OrderParameters<TElement, TKey> orderParameters)
        {
            var i = (source.Count - 1)/2;
            while (i >= 0)
            {
                if (OrderBy.Asc == orderParameters.OrderBy)
                {
                    source.MaxHeapify(i--, source.Count, orderParameters);
                }
                else
                {
                    source.MinHeapify(i--, source.Count, orderParameters);
                }
            }
        }

        private static void MaxHeapify<TElement, TKey>(this List<TElement> source, int i, int heapSize,
            OrderParameters<TElement, TKey> orderParameters)
        {
            var left = Left(i);
            var right = Right(i);
            var largest = i;
            if (left < heapSize &&
                orderParameters.Comparer.IsFirstGreaterThenSecond(orderParameters.Selector(source[left]),
                    orderParameters.Selector(source[largest])))
            {
                largest = left;
            }

            if (right < heapSize &&
                orderParameters.Comparer.IsFirstGreaterThenSecond(orderParameters.Selector(source[right]),
                    orderParameters.Selector(source[largest])))
            {
                largest = right;
            }

            if (largest == i) return;

            source.Swap(i, largest);
            source.MaxHeapify(largest, heapSize, orderParameters);
        }

        private static void MinHeapify<TElement, TKey>(this List<TElement> source, int i, int heapSize,
            OrderParameters<TElement, TKey> orderParameters)
        {
            var left = Left(i);
            var right = Right(i);
            var largest = i;
            if (left < heapSize &&
                orderParameters.Comparer.IsFirstLessThenSecond(orderParameters.Selector(source[left]),
                    orderParameters.Selector(source[largest])))
            {
                largest = left;
            }

            if (right < heapSize &&
                orderParameters.Comparer.IsFirstLessThenSecond(orderParameters.Selector(source[right]),
                    orderParameters.Selector(source[largest])))
            {
                largest = right;
            }

            if (largest == i) return;

            source.Swap(i, largest);
            source.MinHeapify(largest, heapSize, orderParameters);
        }
    }
}