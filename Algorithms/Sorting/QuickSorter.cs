﻿using System;
using System.Collections.Generic;
using Algorithms.Common;
using Algorithms.Extensions;

namespace Algorithms.Sorting
{
    public static class QuickSorter
    {
        public static void QuickSort<TElement, TKey>(
            this List<TElement> source, Func<TElement, TKey> selector,
            OrderBy orderBy = OrderBy.Asc, IComparer<TKey> comparer = null)
        {
            var sortInfo = new OrderParameters<TElement, TKey>
            {
                OrderBy = orderBy,
                Selector = selector,
                Comparer = comparer ?? Comparer<TKey>.Default
            };
            Sort(source, 0, source.Count - 1, sortInfo);
        }

        private static void Sort<TElement, TKey>(List<TElement> source, int lowIndex, int hightIndex,
            OrderParameters<TElement, TKey> orderParameters)
        {
            if (lowIndex >= hightIndex) return;
            var partitionIndex = Partition(source, lowIndex, hightIndex, orderParameters);
            Sort(source, lowIndex, partitionIndex - 1, orderParameters);
            Sort(source, partitionIndex + 1, hightIndex, orderParameters);
        }

        private static int Partition<TElement, TKey>(List<TElement> source, int lowIndex, int hightIndex,
            OrderParameters<TElement, TKey> orderParameters)
        {
            TKey partition = orderParameters.Selector(source[hightIndex]);
            var i = lowIndex - 1;
            for (int j = lowIndex; j < hightIndex; j++)
            {
                if ((orderParameters.OrderBy == OrderBy.Asc &&
                     orderParameters.Comparer.IsFirstLessThenSecond(orderParameters.Selector(source[j]), partition))
                    ||
                    (orderParameters.OrderBy == OrderBy.Desc &&
                     orderParameters.Comparer.IsFirstGreaterThenSecond(orderParameters.Selector(source[j]), partition)))
                {
                    i++;
                    source.Swap(i, j);
                }
            }
            i++;
            source.Swap(i, hightIndex);
            return i;
        }
    }
}