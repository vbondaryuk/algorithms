﻿using System;
using System.Collections.Generic;
using Algorithms.Common;
using Algorithms.Extensions;

namespace Algorithms.Sorting
{
    public static class MergeSorter
    {
        public static void MergeSort<TElement, TKey>(
            this List<TElement> source, Func<TElement, TKey> selector,
            OrderBy orderBy = OrderBy.Asc, IComparer<TKey> comparer = null)
        {
            var sortInfo = new OrderParameters<TElement, TKey>
            {
                OrderBy = orderBy,
                Selector = selector,
                Comparer = comparer ?? Comparer<TKey>.Default
            };
            Merge(source, 0, source.Count - 1, sortInfo);
        }

        private static void Merge<TElement, TKey>(List<TElement> source, int leftIndex, int rightIndex,
            OrderParameters<TElement, TKey> orderParameters)
        {
            if (leftIndex >= rightIndex)
                return;
            var middleIndex = leftIndex + (rightIndex - leftIndex) / 2;
            Merge(source, leftIndex, middleIndex, orderParameters);
            Merge(source, middleIndex + 1, rightIndex, orderParameters);
            InternalMerge(source, leftIndex, middleIndex, rightIndex, orderParameters);
        }

        private static void InternalMerge<TElement, TKey>(List<TElement> source, int leftIndex, int middleIndex,
            int rightIndex, OrderParameters<TElement, TKey> orderParameters)
        {
            int leftArrayCount = middleIndex - leftIndex + 1;
            int rightArrayCount = rightIndex - middleIndex;
            var leftList = source.GetRange(leftIndex, leftArrayCount);
            var rightList = source.GetRange(middleIndex + 1, rightArrayCount);
            int leftTempIndex = 0;
            int rightTempIndex = 0;
            int currentTempIndex = leftIndex;
            while (leftTempIndex < leftArrayCount && rightTempIndex < rightArrayCount)
            {
                if ((orderParameters.OrderBy == OrderBy.Asc &&
                     orderParameters.Comparer.IsFirstLessThenSecond(orderParameters.Selector(leftList[leftTempIndex]),
                         orderParameters.Selector(rightList[rightTempIndex])))
                    ||
                    (orderParameters.OrderBy == OrderBy.Desc &&
                     orderParameters.Comparer.IsFirstGreaterThenSecond(
                         orderParameters.Selector(leftList[leftTempIndex]),
                         orderParameters.Selector(rightList[rightTempIndex]))))
                {
                    source[currentTempIndex] = leftList[leftTempIndex];
                    leftTempIndex++;
                }
                else
                {
                    source[currentTempIndex] = rightList[rightTempIndex];
                    rightTempIndex++;
                }
                currentTempIndex++;
            }
            while (leftTempIndex < leftArrayCount)
            {
                source[currentTempIndex] = leftList[leftTempIndex];
                leftTempIndex++;
                currentTempIndex++;
            }
            while (rightTempIndex < rightArrayCount)
            {
                source[currentTempIndex] = rightList[rightTempIndex];
                rightTempIndex++;
                currentTempIndex++;
            }
        }
    }
}