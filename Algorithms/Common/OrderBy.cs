﻿namespace Algorithms.Common
{
    public enum OrderBy
    {
        Asc,
        Desc
    }
}