﻿using System;
using System.Collections.Generic;

namespace Algorithms.Common
{
    internal sealed class OrderParameters<TElement, TKey>
    {
        public OrderBy OrderBy { get; set; }
        public IComparer<TKey> Comparer { get; set; }
        public Func<TElement, TKey> Selector { get; set; }
    }
}